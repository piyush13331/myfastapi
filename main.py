from typing import Optional
from fastapi import FastAPI

app=FastAPI()
list_of_items =[]
@app.get("/")
def read_root():
    return {"Hello":"World"}

@app.get("/items/{item_id}")
def read_items(item_id: int,q:Optional[str]= None):
    print("Item id is ",item_id)
    return "done"

@app.get("/items")
def get_all_items():
    return list_of_items

@app.post("/items/{item_name}")
def add_item_name(item_name:str):
    list_of_items.append(item_name)